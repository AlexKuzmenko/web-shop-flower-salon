<?php
    
    define("ROOT", __DIR__);
    
    include(ROOT . "/config/config.php");
    include(ROOT . "/core/autoload.php");
    
    (new Application())->run();