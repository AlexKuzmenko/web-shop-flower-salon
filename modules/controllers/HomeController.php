<?php
class HomeController extends FrontController
{
    
    public function actionIndex($parameters = [])
    {
        $params["categories"] = CategoryModel::getCategories();
        $content = (new View('home/home', $params))->getHTML();
        $this->view->setParam("content", $content);
    }
    
    public function actionPageNotFound($parameters = [])
    {
        $this->view->setParam("title", "Сторінку не знайдено");
        $this->view->setParam("content", "Тут буде шаблон сторінки 404");
    }
}
