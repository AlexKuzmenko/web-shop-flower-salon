<?php

class CategoryController extends FrontController 
{   
    
    public function actionView($parameters = []) 
    {       
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $category->products = Product::getProducts($category_id);
        $params["category"] = $category;
        $params["categories"] = Category::getCategories();
        $content = (new View('category/view', $params))->getHTML();
        
        $this->view->setParam("title", $category->name);
        $this->view->setParam("content", $content);
    } 

    public function actionList($parameters = []) 
    {
        $categories = Category::getCategories();
        $params["categories"] = $categories;
        $content = (new View('category/list', $params))->getHTML();
        $this->view->setParam("title", "Список категорій");
        $this->view->setParam("content", $content);       
    }    
}
