<div class="row">
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= $product->name ?>
            </div>
            <div class="panel-body">
                <div class="media">
                    <div class="media-left">
                        <p>
                            <a href="<?= $product->imageFiles['600_image']; ?>"><img src="<?= $product->imageFiles['200_image']; ?>" alt=""  class="media-object"></a>
                        </p>
                        <p class="text-center">
                            <button id="add_<?= $product->product_id ?>" onclick="addToCart(this);" class="btn btn-default btn-primary" data-toggle="modal" data-target="#myModal">
                                Додати
                            </button>
                        </p>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Опис</h4>
                      <p><?= $product->description; ?></p>
                    </div>
                </div>

                <div class="list-group">
                    <div class="list-group-item list-group-item-success">Атрибути</div>
                    <div class="list-group-item">
                        <strong>Ціна:</strong> <?= $product->price; ?>
                    </div>
                    <div class="list-group-item">
                        <strong>Категорія:</strong> <?= $product->category_name; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Товар додано до кошика</h4>
      </div>
      <div class="modal-body">
          <p>Всього в кошику <span class='cart_quantity'></span> товарів</p>
          <p>Загальною вартістю <span class='cart_price'></span> грн</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Продовжити покупки</button>
        <a href="/cart" class="btn btn-primary">Перейти до кошика</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    
   function addToCart(obj) {
       var id = obj.id;
       var reg = /^add_([\d]+)$/;
       var res = reg.exec(id);
       $.ajax({
           type: "POST",
           url: "/product/addToCart",
           data: {
               "product_id": res[1],
           },
           success: function(data) {
               var obj = JSON.parse(data);
               $(".cart_quantity").text(obj.quantity);
               $(".cart_price").text(obj.price);
           }
        });
       
    }
</script>
