<div class="list-group">
    <span class="list-group-item list-group-item-success">Товари</span>
    <?php foreach($products as $key => $product): ?>
        <a href="/product/<?php echo $product['product_id']; ?>" class="list-group-item bg-primary">
            <span><?php echo $product["name"]; ?></span>
        </a>
    <?php endforeach;?>
</div>
