<h1>Оформлення замовлення</h1>
<div class="row">
    <div class="col-xs-12 col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Дані кошика</h3>
            </div>
            <div class="panel panel-body">
                <?php if ($cart['products']): ?>
                <table class="table table-bordered">
                    <tr class="bg-primary">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total Price</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($cart['products'] as $product): ?>
                    <tr class="<?php if ($product['quantity'] == 0): ?>danger<?php endif; ?>">
                        <td><?= $product['product_id'] ?></td>
                        <td><?= $product['name'] ?></td>
                        <td>
                            <div class="form-group">
                                <input type="number" min="0" class="form-control input-sm" name="quantity_<?= $product['product_id'] ?>" value="<?= $product['quantity'] ?>">
                            </div>
                        </td>
                        <td><?= $product['price'] ?></td>
                        <td><span class="itemTotalPrice_<?= $product['product_id'] ?>"><?= $product['totalprice'] ?></span> грн.</td>
                        <td>
                            <span class="btn btn-default glyphicon glyphicon-plus btn-xs" onclick="updateCart(1, <?= $product['product_id'] ?>)"></span>
                            <span class="btn btn-default glyphicon glyphicon-minus btn-xs" onclick="updateCart(-1, <?= $product['product_id'] ?>)"></span>
                            <span class="btn btn-default glyphicon glyphicon-remove btn-xs" onclick="updateCart(0, <?= $product['product_id'] ?>)"></span>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <tr class="info">
                        <td class="text-right" colspan="2">TOTAL</td>
                        <td><span class='cart_quantity'><?= $cart['totalQuantity'] ?></span> од.</td>
                        <td></td>
                        <td><span class='cart_price'><?= $cart['totalPrice'] ?></span> грн.</td>
                        <td></td>
                    </tr>
                </table>
                <?php else: ?>
                    <h3>Кошик порожній!</h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Форма замовлення</h3>
            </div>
            <div class="panel panel-body">
                <div class="form-group">
                    <label for="form-firstname">First Name</label>
                    <input type="text" id="form-firstname" name="firstname" class="form-control" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="form-lastname">Last Name</label>
                    <input type="text" id="form-lastname" name="lastname" class="form-control" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="form-email">Email</label>
                    <input type="email" id="form-email" name="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="form-phone">Phone Number: (XXX)XXX-XXXX</label>
                    <input type="tel" id="form-phone" name="phone" class="form-control" placeholder="(XXX)XXX-XXXX">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
        <input type="submit" class="btn btn-success btn-lg" onclick="makeOrder()" value="Оформити замовлення">
    </div>
</div>


<script>
    function updateCart(q, productId) {
        var quantityElement = $("[name=quantity_" + productId + "]");
        var closestTR = quantityElement.closest("tr");
        var quantity = parseInt(quantityElement.val());
        quantity += q;
        if (quantity < 0 || q == 0) quantity = 0;
        if (quantity == 0) {
            closestTR.addClass('danger');
        } else {
            closestTR.removeClass('danger');
        };
        $.ajax({
            type: 'POST',
            url: 'product/updateCart',
            data: {
              'product_id': productId,
              'quantity': quantity,
            },
            success: function(data) {
                var cart = JSON.parse(data);
                quantityElement.val(cart.itemQuantity);
                $(".itemTotalPrice_" + productId).text(cart.itemTotalPrice);
                $(".cart_quantity").text(cart.quantity);
                $(".cart_price").text(cart.price);
            }
        });
    }
    
    function makeOrder() {

        var error = [];

        if (!/^[A-Za-zА-Яа-яІіЇїЄє]{2,20}$/.test($("#form-firstname").val())) {
            error.push("Невірно введене ім'я");
        }
        if (!/^[A-Za-zА-Яа-яІіЇїЄє]{2,20}$/.test($("#form-lastname").val())) {
            error.push("Невірно введене прізвище");
        }
        if (!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test($("#form-email").val())) {
            error.push("Невірно введений email");
        }
        if (!/^\(\d{3}\)\d{3}-\d{4}$/.test($("#form-phone").val())) {
            error.push("Невірно введений номер телефону");
        }
        if (error.length > 0) {
            for (var i = 0; i < error.length; i++) {
                error[i] = "<div class='list-group-item list-group-item-danger'>" + error[i] + "</div>";
            }
            var errorString = error.join('');
            $('#errors').html(errorString);
            $('#myModalErrors').modal('show');
        } else {
            $.ajax({
                url: 'product/getCart',
                success: function(data) {
                    var cart = JSON.parse(data);
                    if (cart.totalQuantity == 0) {
                        var errorString = "<div class='list-group-item list-group-item-danger'>В кошику немає товарів</div>";
                        $('#errors').html(errorString);
                        $('#myModalErrors').modal('show');
                    } else {
                        var cartItems = [];
                        cartItems.push("<table class='table table-bordered'>");
                        cartItems.push("<tr class='active'>" +
                                       "<th>ID</th>" + 
                                       "<th>Name</th>" + 
                                       "<th>Quantity</th>" + 
                                       "<th>Price</th>" + 
                                       "<th>Total Price</th>" +
                                       "</tr>");                        
                        for (var i = 0; i < cart.products.length; i++) {
                            var product = cart.products[i];
                            if (product.quantity > 0) {
                            cartItems.push("<tr>" +
                                           "<td>" + product.product_id + "</td>" + 
                                           "<td>" + product.name + "</td>" + 
                                           "<td>" + product.quantity + "</td>" + 
                                           "<td>" + product.price + "грн.</td>" + 
                                           "<td>" + product.totalprice + "грн.</td>" +
                                           "</tr>");
                            }
                        }
                        cartItems.push("<tr class='info'>" +
                                           "<th></th>" + 
                                           "<th>Всього: </th>" + 
                                           "<th>" + cart.totalQuantity + "</th>" + 
                                           "<th>На суму: </td>" + 
                                           "<th>" + cart.totalPrice + "грн.</th>" +
                                           "</tr>");
                        cartItems.push("</table>");
                        var cartString = cartItems.join('');
                        $("#cart-data").html(cartString);
                        $("#firstname-data").html($("#form-firstname").val());
                        $("#lastname-data").html($("#form-lastname").val());
                        $("#email-data").html($("#form-email").val());
                        $("#phone-data").html($("#form-phone").val());
                        $('#myModalCart').modal('show');
                    }
                }
            });
        }
    }
    
    function sendOrder() {
        var orderData = $("#order-data").html();
        $.ajax({
                type: 'POST',
                data: {
                  'order': orderData,
                },
                url: 'product/sendOrder',
                success: function(data) {
                    if (data) {
                        $('#myModalSuccess').modal('show');
                    }
                }
            });
    }
</script>

<div class="modal fade" id="myModalErrors" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Помилка!</h4>
      </div>
      <div class="modal-body">
        <div class="list-group" id="errors">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Підтвердіть надсилання даних замовлення</h4>
      </div>
      <div class="modal-body" id="order-data">
        <div id="cart-data"></div>
        <div class="list-group">
            <div class='list-group-item'>Прізвище та ім'я: <strong><span id='firstname-data'></span> <span id='lastname-data'></span></strong></div>
            <div class='list-group-item'>Email: <strong><span id='email-data'></span></strong></div>
            <div class='list-group-item'>Phone Number: <strong><span id='phone-data'></span></strong></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-success" data-dismiss="modal" onclick="sendOrder();">Надіслати</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Змінити дані замовлення</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Вітаємо!</h4>
      </div>
      <div class="modal-body">
          <div class="well well-lg success">Дані замовлення успішно надіслано!</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>