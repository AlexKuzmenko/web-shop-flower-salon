<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/modules/views/css/style.css">
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#"><img src="/images/logo.png"></a>
        <h3 class="navbar-text">Квітковий салон</h3>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/">Головна <span class="sr-only">(current)</span></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Категорії <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php foreach ($categories as $category): ?>  
                <li><a href="/category/<?= $category['category_id']; ?>"><?= $category['name']; ?></a></li>
            <?php endforeach; ?>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span class="badge"><span class='cart_quantity'><?php echo $cartTotals['quantity']; ?></span></span>
          </a>
          <ul class="dropdown-menu">
              <li><a href="/cart">Всього товарів в кошику: <span class='cart_quantity'><?php echo $cartTotals['quantity']; ?></span></a></li>
              <li><a href="/cart">На загальну вартість: <span class='cart_price'><?php echo $cartTotals['price']; ?></span></a></li>
              <li role="separator" class="divider"></li>
              <li><a href="/cart">Перейти до кошика</a></li>
             <!-- <li><a href="#" onclick="deleteSession(this);">Очистити кошик</a></li>  -->
          </ul>
        </li>
      </ul> 
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container content">
    <?= $content ?>
</div>

<script>
    function deleteSession() {
            $.ajax({
            type: "POST",
            url: "/product/deleteSession",
            success: function(data) {
                var obj = JSON.parse(data);
                $(".cart_quantity").text(obj.quantity);
                $(".cart_price").text(obj.price);
            }
        });
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
