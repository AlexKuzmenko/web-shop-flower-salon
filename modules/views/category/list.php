<div class="list-group">
    <span class="list-group-item list-group-item-success">Категорії</span>
    <?php foreach($categories as $key => $category): ?>
        <a class="list-group-item <?php if ($category['category_id'] == $category->category_id): ?>active<?php endif; ?>" href="/category/<?= $category['category_id']; ?>">
            <?php echo $category["name"]; ?>
        </a>
    <?php endforeach;?>
</div>