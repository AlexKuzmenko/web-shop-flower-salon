<section id="catalog">
    <h2 class="text-center">Інтернет-магазин "Квітковий салон"</h2>
    <p class="text-center">Каталог</p>
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <div class="container-fluid">
                <div class="row">
                    <?php foreach ($categories as $category): ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="panel panel-success text-center">
                            <div class="panel-heading">
                                <a href="/category/<?= $category['category_id']; ?>"><?= $category['name']; ?></a>
                            </div>
                            <div class="panel-body">
                                <a href="/category/<?= $category['category_id']; ?>">
                                    <img src="<?= $category['image_files']['200_image'] ?>" class="img-thumbnail" alt="Cinque Terre" width="200px">
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="services">
<h2 class="text-center">Послуги Квіткового салону</h2>
<p class="text-center">Якість обумовлена досвідом</p>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-2 left">
            <div class="resume_item">
                <h3>Вирощування</h3>
                <div class="resume_icon resume-icon-1"></div>
                <p>Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. </p>
            </div>
            <div class="resume_item">
                <h3>Озеленення</h3>
                <div class="resume_icon resume-icon-2"></div>
                <p>"Риба" не тільки успішно пережила п'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset</p>
            </div>
        </div>
        <div class="col-md-4 offset-md-4 right">
            <div class="resume_item">
                <h3>Догляд</h3>
                <div class="resume_icon resume-icon-3"></div>
                <p>Вже давно відомо, що читабельний зміст буде заважати зосередитись людині, яка оцінює композицію сторінки. Сенс використання Lorem Ipsum полягає в тому, що цей текст має більш-менш нормальне розподілення літер на відміну від.</p>
            </div>
            <div class="resume_item">
                <h3>Торгівля</h3>
                <div class="resume_icon resume-icon-4"></div>
                <p>Це робить текст схожим на оповідний. Багато програм верстування та веб-дизайну використовують Lorem Ipsum як зразок і пошук за терміном "lorem ipsum" відкриє багато веб-сайтів, які знаходяться ще в зародковому стані.</p>
            </div>
        </div>
    </div>
</div>
</section>