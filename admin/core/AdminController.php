<?php
class AdminController extends Controller
{   
    protected $admin;
    
    public function __construct() 
    {
        if (isset($_SESSION['admin'])) {
            $this->admin = $_SESSION['admin'];
        }
        $params = [];
        $params['categories'] = Category::getCategories();
        $params['admin'] = $this->admin;
        $this->view = new View('index', $params);
    }

}
