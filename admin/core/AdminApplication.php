<?php
class AdminApplication extends Application
{   
    private $admin;
    
    protected function init()
    {
        DataBase::Connect();
        session_start();
        $this->admin = $_SESSION['admin'];
    }


    protected function action() 
    {
        if (!$this->admin && $this->controllerClass !== "HomeController") {
            header("Location: /admin/login");
            die;
        }
        $className = $this->controllerClass;
        $action = $this->actionMethod;
        $this->controller = new $className;
        $this->controller->$action($this->parameters);
    }
    
}
