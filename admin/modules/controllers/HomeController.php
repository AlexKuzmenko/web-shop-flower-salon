<?php
class HomeController extends AdminController
{
    const ADMIN_USER = "admin";
    const ADMIN_PASSWORD = "1";
    
    public function actionIndex($parameters = []) 
    {
        header("Location: /admin/login");
        die;
    }
    
    public function actionLogin($parameters = []) 
    {
        if ($_SESSION["admin"]) {
            header("Location: /admin/products");
            die;
        }
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if ($_POST["login"] == self::ADMIN_USER && 
                $_POST["password"] == self::ADMIN_PASSWORD) {
                    session_start();
                    $_SESSION["admin"] = true;
                    header("Location: /admin/products");
                    die;
                } else {
                    self::$logs['errors'][] = "Неуспішний вхід. Спробуйте ще раз.";
                }
        }
        $content = (new View('home/login_form'))->getHTML();
        $this->view->setParam("title", "Форма для входу");
        $this->view->setParam("content", $content);
    }
    
    public function actionLogout($parameters = []) 
    {
        session_destroy();
        header("Location: /admin/login");
        die;
    }
    
    public function actionPage404($parameters = [])
    {
        $this->view->setParam("title", "Сторінка 404");
        $this->view->setParam("content", "Тут буде шаблон сторінки 404");
    }
}
