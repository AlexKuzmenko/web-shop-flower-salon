<?php

class ProductController extends AdminController 
{
    
    public function actionSubmit($parameters = []) 
    {
        $product = new ProductModel;
        $action = $_POST['action'];
        $array = $_POST;
        $product->initObjectFromArray($array);
        $category_id = (isset($_POST['category_id']))?$_POST['category_id']:'';
        if ($action == 'edit') {
            $product->update();
        } elseif ($action == 'create') {
            $product->create();
        } elseif (in_array($action, ["activate", "deactivate"])) {
            $status = (int)($action == 'activate');
            ProductModel::changeStatus($status);
        }
        header("Location: /admin/products/$category_id");
        die;
    }
    
    public function actionView($parameters = []) 
    {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $params["product"] = $product;   
        $content = (new View('product/view', $params))->getHTML();
        $this->view->setParam("title", $product->title);
        $this->view->setParam("content", $content);
    }
    
    public function actionEdit($parameters = []) 
    {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $categories = CategoryModel::getCategories();
        $params["product"] = $product;
        $params["categories"] = $categories;
        $params["action"] = "edit";
        $content = (new View('product/form', $params))->getHTML();        
        $this->view->setParam("title", "Редагування товару");
        $this->view->setParam("content", $content);
    }

    public function actionAdd($parameters = []) 
    {
        $categories = CategoryModel::getCategories();
        $params["categories"] = $categories;
        $params["action"] = "create";
        $content = (new View('product/form', $params))->getHTML();
        $this->view->setParam("title", "Додавання товару");
        $this->view->setParam("content", $content);
    }
    
    public function actionList($parameters = []) 
    {
        $category_id = (isset($parameters[0]))?$parameters[0]:0;
        $params['products'] = ProductModel::getProducts($parameters[0]);
        $params['categories'] = CategoryModel::getCategories();
        $params['category_id'] = $category_id;
        $content = (new View('product/list', $params))->getHTML();
        $this->view->setParam("title", "Список товарів");
        $this->view->setParam("content", $content);       
    }
   
}
