<?php

class ProductModel extends Product 
{   
    public function update() 
    {
        $oldImage = $this->getImage();
        if ($newImage = Image::setImage('product/' . $this->product_id, $oldImage)) {
            $this->image = $newImage;
        } else {
            $this->image = $oldImage;
        };
        $sql = "UPDATE product SET "
                . "name=?, "
                . "description=?, "
                . "category_id=?, "
                . "sku=?, "
                . "image=?, "
                . "price=?, "
                . "status=?,"
                . "url=?, "
                . "meta_description=?, "
                . "meta_keyword=? "
                . "WHERE product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->category_id,
            $this->sku,
            $this->image,
            $this->price,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
            $this->product_id,
        ]);
        
        return $this->product_id;
    }

    public function create() 
    {
        $sql = "INSERT INTO product("
                . "category_id, "
                . "name, "
                . "description, "
                . "url, "
                . "status, "
                . "meta_description, "
                . "meta_keyword) "
                . "VALUES (?, ?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->category_id,
            $this->name,
            $this->description,
            $this->url,
            $this->status,
            $this->meta_description,
            $this->meta_keyword
        ]);
        $this->product_id = DataBase::handler()->lastInsertId();
        $this->image = Image::setImage('product/' . $this->product_id);
        $this->updateImage();
        return $this->product_id;
    }
    
    public static function changeStatus($status) 
    {
        if (isset($_POST['product']) && !empty($_POST['product'])) {
            $products = $_POST['product'];
            $str = join(", ", $products);
            $sql = "UPDATE product SET status=" . (int)$status . " WHERE product_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
  
    private function updateImage()
    {
        $sql = "UPDATE product SET image=? WHERE product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->image,
            $this->product_id,
        ]);
        return $this->product_id;        
    }
    
    private function getImage()
    {
        $sql = "SELECT image FROM product WHERE product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$this->product_id]);
        $result = $stmt->fetch();
        return $result['image'];
    }
}
