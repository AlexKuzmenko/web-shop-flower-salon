<div class="row">   
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <form action="/admin/category/submit" method="post">
            <table class="table table-bordered">
                <tr class="success">
                    <th>Check</th>
                    <th>Image</th>
                    <th>Category Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($categories as $key => $category): ?>
                <tr class="<?php if (!$category['status']): ?>danger<?php endif; ?>">
                    <td>
                        <input type="checkbox" name="category[]" value="<?= $category['category_id']; ?>">
                    </td>
                    <td>
                        <span class="thumb"><img src="<?= $category['image_files']['100_image']; ?>"></span>
                    </td>
                    <td>
                        <span><?= $category['name']; ?></span>
                    </td>
                    <td>
                        <a href="/admin/category/edit/<?= $category['category_id']; ?>" class="btn btn-default pull-right">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
            <div class="form-group">
                <label for="form-action">Виберіть дію</label>
                <select name="action" id="form-action" class="form-control">
                    <option value="deactivate">Деактивувати</option>
                    <option value="activate">Активувати</option>
                </select>
            </div>
            <input type="hidden" name="category_id" value="<?= $category_id ?>">
            <div class="form-group">
                <input type="submit" value="Виконати" class="btn btn-default">
            </div>
        </form>
    </div>
</div>