<div class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <h2>Форма "Категорія"</h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php if ($action == 'edit'): ?>
                    Редагування категорії
                <?php else: ?>
                    Створення нової категорії
                <?php endif; ?>
            </div>
            <div class="panel-body">
                <form method='post' action='/admin/category/submit' enctype="multipart/form-data">   
                    <div class="form-group">
                        <label for="form-category-id">Category ID</label>
                        <input type='text' class="form-control"  readonly="readonly" name='category_id' id="form-category-id" value='<?php echo $category->category_id; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-category-name">Category Name</label>
                        <input type='text' class="form-control"  name='name' id="form-category-name" value='<?php echo $category->name; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-description">Category Description</label>
                        <textarea name='description' id="form-description" class="form-control" ><?php echo $category->description; ?></textarea>
                    </div>

                    <div>
                        <img src="<?= $category->imageFiles['200_image'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="form-category-image">Category Image</label>
                        <input type='file' class="form-control"  name='image' id="form-category-image">
                    </div>

                    <div class="form-group">
                        <label for="form-status">Status</label>
                        <?php if ($category->status): ?>
                        <input type='checkbox' class="form-control"  name='status' id="form-status" value="1" checked="checked">
                        <?php else: ?>
                        <input type='checkbox' class="form-control" name='status' value="1">
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label for="form-category-url">Category url</label>
                        <input type='text' class="form-control" name='url' id="form-category-url" value='<?php echo $category->url; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-meta-description">META description</label>
                        <input type='text' class="form-control" name='meta_description' id="form-meta-description" value='<?php echo $category->meta_description; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-meta-keyword">META keywords</label>
                        <input type='text' class="form-control" name='meta_keyword' id="form-meta-keyword" value='<?php echo $category->meta_keyword; ?>'>
                    </div>

                    <div class="form-group">
                        <input type='hidden' name='action' value='<?php echo $action; ?>'>
                        <input type='submit' class="btn btn-default" value="Створити">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>                