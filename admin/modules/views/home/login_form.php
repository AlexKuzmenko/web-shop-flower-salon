<div class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <h1>Форма для входу</h1>
        <form action="/admin/login" method="post">
            <div class="form-group">
                <input type="text" name="login" class="form-control" placeholder="login">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="password">
            </div>
            <input type="submit" class="btn btn-danger" value="Увійти">
        </form>
    </div>
</div>