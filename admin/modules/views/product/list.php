<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-3">   
        <div class="list-group">
            <span class="list-group-item list-group-item-success">Категорії</span>
            <?php foreach($categories as $key => $categoryItem): ?>
                <a class="list-group-item <?php if ($categoryItem['category_id'] == $category_id): ?>active<?php endif; ?>" href="/admin/products/<?= $categoryItem['category_id']; ?>">
                    <span><?= $categoryItem["name"]; ?></span>
                </a>
            <?php endforeach;?>
        </div>
    </div>
    
    <div class="col-xs-12 col-md-8 col-lg-9">
        <form action="/admin/product/submit" method="post">
            <table class="table table-bordered">
                <tr class="success">
                    <th>Check</th>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Action</th>
                </tr>
                <?php foreach($products as $key => $product): ?>
                <tr class="<?php if (!$product['status']): ?>danger<?php endif; ?>">
                    <td>
                        <input type="checkbox" name="product[]" value="<?= $product['product_id']; ?>">
                    </td>
                    <td>
                        <span class="thumb"><img src="<?= $product['image_files']['100_image']; ?>"></span>
                    </td>
                    <td>
                        <span><?= $product['name']; ?></span>
                    </td>
                    <td>
                        <a href="/admin/product/edit/<?= $product['product_id']; ?>" class="btn btn-default pull-right">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
            <div class="form-group">
                <label for="form-action">Виберіть дію</label>
                <select name="action" id="form-action" class="form-control">
                    <option value="deactivate">Деактивувати</option>
                    <option value="activate">Активувати</option>
                </select>
            </div>
            <input type="hidden" name="category_id" value="<?= $category_id ?>">
            <div class="form-group">
                <input type="submit" value="Виконати" class="btn btn-default">
            </div>
        </form>
    </div>
</div>