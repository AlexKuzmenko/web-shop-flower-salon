<?php
return array(
    "admin"                         => "home/index",
    "admin/login"                   => "home/login",
    "admin/logout"                  => "home/logout",
    "admin/page404"                 => "home/page404",
    "admin/categories"              => "category/list",
    "admin/category/edit/([0-9]+)"  => "category/edit/$1",
    "admin/category/add"            => "category/add",
    "admin/category/([0-9]+)"       => "category/view/$1",
    "admin/category/submit"         => "category/submit",
    
    "admin/products"                => "product/list",
    "admin/products/([0-9]+)"        => "product/list/$1",
    "admin/product/edit/([0-9]+)"   => "product/edit/$1",
    "admin/product/add"             => "product/add",
    "admin/product/([0-9]+)"        => "product/view/$1",
    "admin/product/submit"          => "product/submit",
);
