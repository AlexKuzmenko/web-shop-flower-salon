<?php

class Category extends Model
{
    public $category_id;
    public $name;
    public $description;
    public $image;
    public $imageFiles;
    public $status;
    public $url;
    public $meta_description;
    public $meta_keyword;
    public $products;
    
    public function __construct($category_id = 0) 
    {
        if ($category_id > 0) {
            $categoryArray = $this->getCategoryById($category_id);
            $this->initObjectFromArray($categoryArray);
            $this->imageFiles = $this->setImageFiles();
        }
    }

    public function setImageFiles()
    {
        $sizes = [600, 400, 200, 100];
        $defaultImage = '/images/default.gif';
        $imageFiles = [];
        foreach ($sizes as $size) {
            $fileName = '/images/category/' . $this->category_id . '/' . $size . '_' . $this->image;
            $imageFiles[$size . '_image'] = ($this->image) ? $fileName : $defaultImage;
        }
        return $imageFiles;
    }
    
    public function getCategoryById($category_id) 
    {
        $sql = "SELECT * FROM category WHERE category_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$category_id]);
        return $stmt->fetch();
    }
    

    /**
     * Отримання списку категорій
    */
    public static function getCategories() 
    {
        $stmt = DataBase::handler()->query("SELECT * FROM category;");
        $categories = $stmt->fetchAll();
        $sizes = [600, 400, 200, 100];
        $defaultImage = '/images/default.gif';
        foreach ($categories as &$category) {
            foreach ($sizes as $size) {
                $fileName = '/images/category/' . $category['category_id'] . '/' . $size . '_' . $category['image'];
                $category['image_files'][$size . '_image'] = ($category['image']) ? $fileName : $defaultImage;
            }
        }
        return $categories;
    }  
}
