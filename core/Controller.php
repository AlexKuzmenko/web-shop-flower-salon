<?php
class Controller 
{   
    protected $view;
    protected $log;
      
    public function render()
    {
        $this->view->setParam('log', $this->log);
        $this->view->render();
    }
}
